% Eating Habits
% John Doe
% March 22, 2005

# In the morning

## In the morning

- Eat eggs
- Drink coffee

## How to Eat Eggs

![](https://i.dailymail.co.uk/i/pix/2011/09/29/article-2043180-02DD574A00000578-204_468x286.jpg)

# In the evening

## In the evening
- Eat spaghetti
- Drink wine

# Conclusion

## Conclusion
- And the answer is...
- $f(x)=\sum_{n=0}^\infty\frac{f^{(n)}(a)}{n!}(x-a)^n$    
