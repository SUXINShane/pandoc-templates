---
title: Homework
author:
- name: Jane Doe
  affiliation: A0123456F
  email: jane@example.com
date: 31 January 2018
abstract: 'This is a homework submission.'
keywords: homework; module
authorfootnote: "Delete if required."
bibliography: example.bib
...

Introduction
===============
Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed porttitor massa vestibulum iaculis cursus. Donec sapien lorem, lobortis non gravida eu, ullamcorper sed sem. Curabitur nec posuere lacus. Curabitur finibus, orci in malesuada lacinia, lorem justo maximus risus, vitae mattis turpis elit at dolor. Vestibulum nec metus felis. Phasellus maximus ipsum sit amet dui egestas, at viverra ligula placerat. Phasellus vitae est et velit elementum fermentum vel ut risus. Pellentesque odio tortor, vestibulum non laoreet nec, gravida pretium felis. Nullam maximus tempus dapibus. Mauris nec iaculis orci. Integer congue, quam ac venenatis cursus, risus nunc convallis nunc, eget consequat arcu lorem porttitor nisl. [@heagerty2000time; @pepe2003statistical]


Subsection
---------------
Mauris augue urna, convallis ac dui eu, blandit ornare turpis. Vestibulum aliquam interdum ipsum, non efficitur magna varius eget. Suspendisse non est eu lacus eleifend dapibus. Integer placerat mollis risus vitae finibus. Praesent vulputate id risus et viverra. Morbi dui sem, ornare et odio sed, vulputate mattis nisi. Donec a iaculis lorem. Phasellus neque urna, blandit a feugiat at, ullamcorper non tellus. Sed justo est, finibus quis tortor at, lacinia ultrices ante. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur dick butt ridiculus mus. In volutpat volutpat eros. Praesent orci ligula, tincidunt ac tellus sit amet, pellentesque volutpat ligula. Fusce sed elit et dolor pellentesque consequat tempor ut dolor. Pellentesque vitawe orfci in lorem tincidunt scelerisque a ut sapien. Mauris varius ornare elementum. Vivamus tincidunt eros ac consectetur tincidunt. [@hoerl1970ridge; @zou2005regularization]


Equations
===============

Here is an equation:

$$ f_{X}(x) = \left(\frac{\alpha}{\beta}\right)\left(\frac{x}{\beta}\right)^{\alpha-1}e^{-\left(\frac{x}{\beta}\right)^{\alpha}}; \alpha,\beta,x > 0 $$

Inline equations
-----------------

No problemo: $\phi\{\alpha^\beta\}$

Figures and tables
=================
Tables are everywhere ^[footnote: Well, not everywhere, but you get the point]:

| Left-Aligned  | Center Aligned  | Right Aligned |
| :------------ |:---------------:| -----:|
| Left column 1 | this text       |  $100 |
| Left column 2 | is              |   $10 |
| Left column 3 | centered        |    $1 |

![](figure/fig2-1.png){width=50%}

Conclusion
================
Cras est sapien, ornare sed nulla id, congue lobortis ante. Suspendisse potenti. Ut eleifend diam mauris, a semper augue blandit rhoncus. Quisque bibendum quam diam, non eleifend ante rhoncus et. Cras facilisis volutpat tortor at elementum. Cras sed elementum nisi, eget placerat est. Nulla congue purus lorem, nec mollis sapien varius ut. Nullam tortor risus, blandit eget ipsum sit amet, venenatis consectetur quam. Integer at eros luctus, facilisis sem sit amet, euismod massa.


Bibliography 
===============

\indent

